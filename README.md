# Nvidia-Launcher

Little helper to open applications with nvidia graphics card on systems with bumblebee.

Nvidia-Launcher looks for .desktop files on your system and gets the application name and execute entries out of it.
Then it opens a kdialog window in which you can select the wanted application. Said application is then started with optirun.

You can modify the search path for .desktop files or replace optirun with e.g. primusrun.


## Requirements

following packages are required:
*  kdialog

## Usage

*  download nvidia-launcher script
*  make script executable `chmod +x nvidia-launcher`
*  execute script `./nvidia-launcher`
*  select desired application to launch
*  that's it, said application is launched with your nvidia graphics card

## PKGBUILD

Alternatively one can download the PKGBUILD only and install nvidia-launcher as package.
This will also create a .desktop file for you.